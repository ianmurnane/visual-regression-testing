### Visual Regression Testing
Using wdio-image-comparison-service 

### Notes
Requires chrome and chromedriver versions to match  
If it errors, check the folder - **_images/diff_**

### Setup  
npm i

### Run
npx wdio run wdio.conf.js