describe('Visual Regression Test', () => {
    before(async () => {
        await browser.url('/');
    });

    after(async () => {
        await browser.deleteSession();
    });

    it('Should load', async () => {
        expect(await browser.checkFullPageScreen('full-screen')).toEqual(0);
    });
});
