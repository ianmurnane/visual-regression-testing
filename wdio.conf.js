const { join } = require('path');
exports.config = {
    runner: 'local',
    specs: [
        './test/**/*.js'
    ],
    exclude: [],
    maxInstances: 1,
    capabilities: [{
        maxInstances: 1,
        browserName: 'chrome',
        acceptInsecureCerts: true,
        'goog:chromeOptions': {
            args: [
                '--no-sandbox',
                '--headless',  // No visible browser
                '--start-maximize',
                '--window-size=1280x1024',
                '--log-level=3',  // INFO = 0, WARNING = 1, LOG_ERROR = 2, LOG_FATAL = 3
            ],
        },
    }],
    // Level of logging verbosity: trace | debug | info | warn | error | silent
    logLevel: 'silent',
    bail: 0,
    baseUrl: 'https://www.google.com',
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    services: [['chromedriver'], ['image-comparison', {
        // https://webdriver.io/docs/options
        actualFolder: join(process.cwd(), './images/action/'),
        baselineFolder: join(process.cwd(), './images/basline/'),
        diffFolder: join(process.cwd(), './images/diff/'),
        formatImageName: '{tag}-{browserName}-{width}x{height}-dpr-{dpr}',
        screenshotPath: join(process.cwd(), './images/'),
        savePerInstance: true,
        autoSaveBaseline: true,
        disableCSSAnimation: true,
        hideScrollBars: true,
        clearRuntimeFolder: true,
        hideElements:[],
        removeElements:[],
        rawMisMatchPercentage: true,
    }]],
    framework: 'mocha',
    reporters: ['spec'],
    port: 4444,
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000
    },
}
